package ru.kuzin.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kuzin.tm.api.endpoint.IAuthEndpoint;
import ru.kuzin.tm.api.endpoint.IUserEndpoint;
import ru.kuzin.tm.dto.model.UserDTO;
import ru.kuzin.tm.exception.entity.UserNotFoundException;
import ru.kuzin.tm.listener.AbstractListener;

@Component
public abstract class AbstractUserListener extends AbstractListener {

    @NotNull
    @Autowired
    public IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    public IAuthEndpoint authEndpoint;

    protected void showUser(final UserDTO user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
    }

    @Override
    public String getArgument() {
        return null;
    }

}