package ru.kuzin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.kuzin.tm.api.service.dto.IProjectDtoService;
import ru.kuzin.tm.api.service.model.IProjectService;
import ru.kuzin.tm.dto.ProjectDTO;
import ru.kuzin.tm.model.CustomUser;
import ru.kuzin.tm.model.Project;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectCollectionRestEndpoint {

    @NotNull
    @Autowired
    private IProjectDtoService projectDTOService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Nullable
    @GetMapping
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public List<ProjectDTO> get(@AuthenticationPrincipal final CustomUser user) {
        return projectDTOService.findAll(user.getUserId());
    }


    @PostMapping
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public void post(
            @AuthenticationPrincipal final CustomUser user,
            @NotNull @RequestBody List<ProjectDTO> projects) {
        projectDTOService.saveAll(user.getUserId(), projects);
    }


    @PutMapping
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public void put(
            @AuthenticationPrincipal final CustomUser user,
            @NotNull @RequestBody List<ProjectDTO> projects) {
        projectDTOService.saveAll(user.getUserId(), projects);
    }


    @DeleteMapping
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public void delete(@AuthenticationPrincipal final CustomUser user,
                       @NotNull @RequestBody List<Project> projects) {
        projectService.removeAll(user.getUserId(), projects);
    }

}