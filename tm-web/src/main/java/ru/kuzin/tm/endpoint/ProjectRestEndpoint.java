package ru.kuzin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.kuzin.tm.api.service.dto.IProjectDtoService;
import ru.kuzin.tm.api.service.model.IProjectService;
import ru.kuzin.tm.dto.ProjectDTO;
import ru.kuzin.tm.model.CustomUser;


@RestController
@RequestMapping("/api/project")
public class ProjectRestEndpoint {

    @NotNull
    @Autowired
    private IProjectDtoService projectDTOService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Nullable
    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public ProjectDTO get(
            @AuthenticationPrincipal final CustomUser user,
            @NotNull @PathVariable("id") String id) {
        return projectDTOService.findOneById(user.getUserId(), id);
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public void post(
            @AuthenticationPrincipal final CustomUser user,
            @NotNull @RequestBody ProjectDTO project) {
        projectDTOService.save(user.getUserId(), project);
    }

    @PutMapping
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public void put(
            @AuthenticationPrincipal final CustomUser user,
            @NotNull @RequestBody ProjectDTO project) {
        projectDTOService.save(user.getUserId(), project);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public void delete(
            @AuthenticationPrincipal final CustomUser user,
            @NotNull @PathVariable("id") String id) {
        projectService.removeOneById(user.getUserId(), id);
    }

}