package ru.kuzin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.kuzin.tm.api.service.dto.ITaskDtoService;
import ru.kuzin.tm.dto.TaskDTO;
import ru.kuzin.tm.model.CustomUser;

@RestController
@RequestMapping("/api/task")
public class TaskRestEndpoint {

    @NotNull
    @Autowired
    private ITaskDtoService taskService;

    @Nullable
    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public TaskDTO get(
            @AuthenticationPrincipal final CustomUser user,
            @NotNull @PathVariable("id") String id) {
        return taskService.findOneById(user.getUserId(), id);
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public void post(
            @AuthenticationPrincipal final CustomUser user,
            @NotNull @RequestBody TaskDTO task) {
        taskService.save(user.getUserId(), task);
    }

    @PutMapping
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public void put(
            @AuthenticationPrincipal final CustomUser user,
            @NotNull @RequestBody TaskDTO task) {
        taskService.save(user.getUserId(), task);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'USER')")
    public void delete(
            @AuthenticationPrincipal final CustomUser user,
            @NotNull @PathVariable("id") String id) {
        taskService.removeOneById(user.getUserId(), id);
    }

}